
import os
import shutil
import subprocess

from charmhelpers.core.hookenv import (
    config,
    status_set,
    open_port,
    application_version_set,
)

from charms.reactive import (
    hook,
    when,
    when_not,
    set_state,
    remove_state,
    is_state,
)

from charmhelpers.core.host import (
    chownr,
    service_restart,
)

from charmhelpers.core.templating import render
from charms.layer import (
    php,
    nginx,
)


@when('php.ready')
@when('silph-web.key.ready')
@when_not('silph-web.installed')
def install_thesilphroad():
    web_path = '/srv/web'

    os.chown('/srv', 1000, 1000)
    chownr('/srv', 'ubuntu', 'ubuntu')

    if os.path.exists(web_path):
        status_set('maintenance', 'updating silph-web code')
        cmd = 'git -C {} pull'.format(web_path)
    else:
        status_set('maintenance', 'cloning silph-web repo')
        repo = 'git@bitbucket.org:thesilphroad/the-silph-road-web-app-repo.git'
        cmd = 'git clone {} {}'.format(repo, web_path)

    subprocess.check_call(['su', '-l', 'ubuntu', '-c', cmd])

    create_paths = {
        'app/tmp/cache/pokedex_cache': 0o777,
        'app/tmp/cache/nest_cache': 0o777,
        'app/tmp/cache/persistent': 0o777,
        'app/tmp/cache/models': 0o777,
        'app/tmp/logs': 0o777,
        'app/webroot/img/user-verification-photos': 0o777,
        'app/Console/cake': 0o777,
    }

    delete_paths = [
        'app/tmp/cache/persistent',
        'app/tmp/cache/models',
    ]

    status_set('maintenance', 'removing cache directories')
    for path in delete_paths:
        shutil.rmtree(os.path.join(web_path, path), ignore_errors=True)

    status_set('maintenance', 'creating cache directories')
    for path, perm in create_paths.items():
        p = os.path.join(web_path, path)
        if os.path.exists(p):
            os.chmod(p, perm)
            continue
        os.makedirs(p, perm)

    with open(os.path.join(web_path, '.git/refs/heads/master')) as f:
        app_ver = f.read()[:7]

    application_version_set(app_ver)

    status_set('maintenance', 'installing cronjobs')
    render(
        source='silph.crontab.j2',
        target='/etc/cron.d/silphweb',
        owner='root',
        group='root',
        perms=0o640,
        context={},
    )

    chownr(web_path, 'www-data', 'www-data')
    set_state('silph-web.installed')


@when_not('silph-web.key.ready')
def key_install():
    cfg = """
host bitbucket.org
  StrictHostKeyChecking no
  identityfile /home/ubuntu/.ssh/deploy_key
"""

    status_set('maintenance', 'adding ssh-deply key')
    with open('/home/ubuntu/.ssh/config', 'w+') as f:
        f.write(cfg)

    with open('/home/ubuntu/.ssh/deploy_key', 'w+') as f:
        f.write(config('ssh-key'))
    os.chmod('/home/ubuntu/.ssh/deploy_key', 0o600)
    os.chown('/home/ubuntu/.ssh/deploy_key', 1000, 1000)
    set_state('silph-web.key.ready')


@when('config.changed.ssh-key')
def update_key():
    status_set('maintenance', 'removing ssh-deploy key')
    remove_state('silph-web.key.ready')


@when('nginx.available')
@when('silph-web.installed')
@when_not('silph-web.nginx.ready')
def setup_vhost():
    nginx.configure_site(
        'silphroad',
        'silphroad-vhost.conf',
        socket=php.socket(),
    )

    service_restart('nginx')
    set_state('silph-web.nginx.ready')


@when_not('silph-web.db.ready')
@when_not('silph-web.cache.ready')
@when('silph-web.nginx.ready')
def heyguys():
    status_set('blocked', 'requires a cache and database connection')


@when('silph-web.db.ready')
@when('silph-web.nginx.ready')
@when_not('silph-web.cache.ready')
def no_cache():
    status_set('blocked', 'requires a cache connection')


@when('silph-web.nginx.ready')
@when('silph-web.cache.ready')
@when_not('silph-web.db.ready')
def no_db():
    status_set('blocked', 'requires a database connection')


@when('silph-web.installed')
@when('database.available')
@when_not('silph-web.db.ready')
def configure_db(db):
    ctx = {'database': db}

    render(
        source='database.php.j2',
        target='/srv/web/app/Config/database.php',
        owner='www-data',
        group='www-data',
        perms=0o640,
        context=ctx,
    )

    set_state('silph-web.db.ready')


@when('silph-web.installed')
@when('cache.available')
@when_not('silph-web.cache.ready')
def configure_cache(redis):
    ctx = {'redis': redis.redis_data()[0]}

    render(
        source='core.php.j2',
        target='/srv/web/app/Config/core.php',
        owner='www-data',
        group='www-data',
        perms=0o640,
        context=ctx,
    )

    set_state('silph-web.cache.ready')


@when('silph-web.nginx.ready')
@when('silph-web.db.ready')
@when('silph-web.cache.ready')
@when_not('silph-web.ready')
def ready_freddie():
    open_port(80)
    status_set('active', 'ready')
    set_state('silph-web.ready')


@when('silph-web.ready')
@when('website.available')
def send_port(http):
    http.configure(80)


@hook('update-status')
def code_update():
    if is_state('silph-web.ready'):
        install_thesilphroad()
        ready_freddie()


@hook('upgrade-charm')
def regenerate_conf():
    install_thesilphroad()
    remove_state('silph-web.cache.ready')
    remove_state('silph-web.ready')
